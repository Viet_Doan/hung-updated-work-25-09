# README #


Things that have been done:
Break down the doBot Solidworks model into links. Unnecessary details such as bolts, nuts and washer are removed. 
Convert links into ply files. The parts’ coordinator is adjusted so the models appear correctly.
Work out the 3d volume for each link. Each link is mathematically represented by a cube. The cubes are then used for collision avoiding.
